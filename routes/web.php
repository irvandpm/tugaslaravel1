<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/home', 'HomeController@form');
// 	// return "Oke bgt";
// 	// return view('home');


// // Route::get('/register', 'AuthController@register');

// Route::get('/register', 'AuthController@register');
// Route::get('/welcome', 'AuthController@welcome');
// Route::POST('/welcome', 'AuthController@welcome_post');

// Route::get('/master',function(){
// 	return view('adminlte.master');
// });

// Route::get('/', function(){
// 	return view('tabel1');
// });

// Route::get('/data-tables', function(){
// 	return view('tabel2');
// });

// Route::get('/pertanyaan/create','PertanyaanController@create');
// Route::post('/pertanyaan','PertanyaanController@store');
// Route::get('/pertanyaan','PertanyaanController@index');
// Route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan_id}','PertanyaanController@update'); //update
// Route::delete('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy');


//membuat route menggunakan metode resource
Route::resource('pertanyaan','PertanyaanController'); //cek masing2 namanya di php artisan route:list

