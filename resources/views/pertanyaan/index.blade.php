@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Daftar Pertanyaan</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<!-- untuk menampilkan kalau data berhasil disimpan -->
			@if(session('success'))
			<div class="alert alert-success">
				{{session('success')}}
			</div>
			@endif
			<!-- /. untuk menampilkan kalau data berhasil disimpan -->

			<!-- cara route pakai name --> 
			<a class="btn btn-primary mb-2" href="{{ route('pertanyaan.create')}}">Buat Pertanyaan Baru</a> 
			<!-- /. cara route pakai name -->

			<!-- cara route normal -->
			<!-- <a class="btn btn-primary mb-2" href="/pertanyaan/create">Buat Pertanyaan Baru</a> -->
			<!-- cara route normal -->
			<table class="table table-bordered">
				<thead>                  
					<tr>
						<th style="width: 10px">#</th>
						<th>Judul</th>
						<th>Pertanyaan</th>
						<th style="width: 40px">Actions</th>
					</tr>
				</thead>
				<tbody>
					<!-- isi data -->
					@forelse ($pertanyaan as $key => $pertanyaan)
					<tr>
						<td> {{ $key + 1 }} </td> <!-- nomor -->
						<td> {{ $pertanyaan->judul}} </td>
						<td> {{ $pertanyaan->isi}} </td>
						<td style = "display: flex;"> 
							
							<!-- cara route pakai name --> 
							<a href="{{ route('pertanyaan.show', ['pertanyaan' => $pertanyaan->id])}}" class="btn btn-info btn-sm">show</a>
							<a href="{{ route('pertanyaan.edit', ['pertanyaan' => $pertanyaan->id])}}" class="btn btn-default btn-sm">edit
							</a>
							<form action="{{ route('pertanyaan.destroy', ['pertanyaan' => $pertanyaan->id])}}" method="POST">
								@csrf
								@method('DELETE')
								<input type="submit" value="delete" class="btn btn-danger btn-sm">
							</form>
							<!-- /. cara route pakai name --> 

							<!-- cara route normal --> 
							<!-- <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">show</a> -->
							<!-- <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm">edit
							</a> -->
							<!-- <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
								@csrf
								@method('DELETE')
								<input type="submit" value="delete" class="btn btn-danger btn-sm">
							</form> --> 
						</td>
					</tr>

					@empty <!-- query builder forelse empty nampilin ket kalau data kosong -->
					<tr>
						<td colspan="4" align="center"> Tidak ada data pertanyaan </td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>
		<!-- /.card-body -->

	</div>
</div>
@endsection