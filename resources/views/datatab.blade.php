@extends('adminlte.master')
@push('links')
<!-- <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css')}}"> --> 
<link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"> 

@endpush


@push('scripts')
<script src="{{ asset ('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset ('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
	$(function () {
		$("#example1").DataTable();
	});
</script>
@endpush

