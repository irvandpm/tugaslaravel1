<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
</head>
<body>
	<form action="welcome" method="POST">
		@csrf
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h3>

		<label for="">First Name:</label> <br> <br>
		<input type="text" name="namadepan"> <br> <br>
		<label for="">Last Name:</label> <br> <br>
		<input type="text" name="namabelakang"> <br> <br> 
		<label for="">Gender:</label> <br> <br>
		<input type="radio" name="jns_kelamin" value="0"> Male <br>
		<input type="radio" name="jns_kelamin" value="1"> Female <br>
		<input type="radio" name="jns_kelamin" value="2"> Other <br> <br>
		<label for="">Nationality:</label> <br><br>
		<select>
			<option>Indonesian</option>
			<option>Singaporean</option>
			<option>Malaysian</option>
			<option>Australian</option>
		</select>
		<br><br>
		<label for="">Language Spoken:</label> <br> <br>
		<input type="checkbox" name="bahasa" value="0"> Bahasa Indonesia <br>
		<input type="checkbox" name="bahasa" value="1"> English <br>
		<input type="checkbox" name="bahasa" value="2"> Other <br><br>
		<label>Bio:</label> <br> <br>
		<textarea cols="25" rows="8"></textarea> <br>
		<input type="Submit" value="Sign Up">
	</form>
</body>
</html>