<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan"; //biar nama tabel sama kaya db (dioverwrite)

    // protected $fillable = ['judul','isi']; //tabel2 mana aja yang boleh diisi
    protected $guarded = []; //semua table boleh diisi. kalau didalam [] ada diisi, berarti yang didalam [] gk boleh diisi

}
