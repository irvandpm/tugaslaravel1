<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
	public function create(){
		return view('pertanyaan.create');
	}

	public function store(Request $request){
	// dd($request->all());
		$request->validate([
			'judul' => 'required|unique:pertanyaan',
			'isi' => 'required'
		]);

		/*
		$query = DB::table('pertanyaan')->insert([
			"judul" => $request["judul"],
			"isi" => $request["isi"]
		]);
		*/

		/*
		// menyimpan data dengan metode save (ORM)
		$pertanyaan = new Pertanyaan;
		$pertanyaan->judul = $request["judul"];
		$pertanyaan->isi = $request["isi"];
		$pertanyaan->save();
		*/


		// menyimpan data dengan metode mass assignment. cek di file pertanyaan.php juga protected $guarded
		$pertanyaan = Pertanyaan::create([
			"judul" => $request["judul"],
			"isi" => $request["isi"]
		]);


		return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan');
		// ->with() untuk menampilkan notif sukses
	}


	public function index(){
		//$pertanyaan = DB::table('pertanyaan')->get(); //select*from pertanyaan;
		// dd($pertanyaan);
		$pertanyaan = Pertanyaan::all(); // menampilkan dengan metode model ORM

		return view('pertanyaan.index', compact('pertanyaan'));
	}

	public function show ($pertanyaan_id) {
		// $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first(); 
		// dd($pertanyaan);
		$pertanyaan = Pertanyaan::find($pertanyaan_id); // menampilkan dengan metode model ORM

		return view('pertanyaan.show', compact('pertanyaan'));
	}

	public function edit($pertanyaan_id){
		// $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
		$pertanyaan = Pertanyaan::find($pertanyaan_id); // menampilkan dengan metode model ORM

		return view('pertanyaan.edit', compact('pertanyaan'));

	}

	public function update($pertanyaan_id, Request $request){
		$request->validate([
			'judul' => 'required|unique:pertanyaan',
			'isi' => 'required'
		]);

		/*
		$query = DB::table('pertanyaan')
		->where('id', $pertanyaan_id)
		->update([
			'judul' => $request['judul'],
			'isi' => $request['isi']
		]);
		*/


		// Update menggunakan eloquents dengan metode mass update
		$query = Pertanyaan::where('id', $pertanyaan_id)
							->update([
								'judul' => $request['judul'],
								'isi' => $request['isi']
		]);

		return redirect('/pertanyaan')->with('success','Update Berhasil Disimpan');	
	}

	public function destroy($pertanyaan_id){
		// $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
		
		// Delete menggunakan eloquents dengan metode mass update
		Pertanyaan::destroy($pertanyaan_id);


		return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus!!!');
	}

}
